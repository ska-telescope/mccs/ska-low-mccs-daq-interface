# Version History

## unreleased

## 1.0.0
* [THORN-17] 1.0.0 release - all MCCS repos

## 0.5.0

* [THORN-12] Add methods for measuring data rate on receiver interface

## 0.4.1

* [SKB-610] Add Config option for StationID.

## 0.4.0

* [MCCS-2173] update DAQ interface to support raw beam and new SPEAD format

## 0.3.0

* [MCCS-2141] Update ska-control-model to 1.0.0

## 0.2.0

* [MCCS-2117] Add `wait_for_ready` flag to initialisation.
* [MCCS-1885] Introduce `cadence` as an argument to bandpass monitor.

## 0.1.1

* [MCCS-1586] Allow passing of metadata from DaqHandler
* [MCCS-1636] Use ska-ser-sphinx-theme for documentation

## 0.1.0

* [MCCS-1597] Add interface code from ska-low-mccs-daq
* [MCCS-1535] Bootstrap repo (with help from ska-cookiecutter-pypackage)
